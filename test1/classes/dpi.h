//
//  dpi.h
//  test1
//
//  Created by Mac on 1/3/13.
//  Copyright (c) 2013 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface dpi : NSObject {
    int xres;
    int yres;
    float diag;
    NSString *resText;
}
@property int xres;
@property int yres;
@property float diag;
@property NSString *resText;

- (void) recount;
@end
