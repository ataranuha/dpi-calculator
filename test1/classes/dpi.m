//
//  dpi.m
//  test1
//
//  Created by Mac on 1/3/13.
//  Copyright (c) 2013 Mac. All rights reserved.
//

#import "dpi.h"
#import "math.h"

@implementation dpi
@synthesize xres, yres, diag, resText;

- (void) recount {
    float dpi = sqrt((yres*yres+xres*xres)/(diag*diag));
    float aspect = (float) yres/xres;
    float heightInch = sqrt((diag*diag)/(aspect*aspect+1));
    float widthInch = heightInch*aspect;
    float heightCm = heightInch*2.53;
    float widthCm = widthInch*2.53;
    float dotPinch = heightCm/yres;    
    resText = [[NSString alloc] initWithFormat:@"Display size: %.2f\" × %.2f\" (%.2fcm × %.2fcm) = %.2f DPI(PPI), %.4fmm dot pitch, %.1f PPI²",heightInch,widthInch,heightCm,widthCm,dpi,dotPinch,dpi*dpi];
}
@end