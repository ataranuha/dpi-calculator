//
//  resItem.h
//  test1
//
//  Created by Mac on 1/20/13.
//  Copyright (c) 2013 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface resItem : NSObject

@property NSString *name, *resx, *resy, *aspect, *resolution;

@end
