//
//  ViewController.h
//  test1
//
//  Created by Mac on 12/26/12.
//  Copyright (c) 2012 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "resItem.h"
#import <Foundation/Foundation.h>
#import "resListController.h"
#import "displaySizes.h"

@interface ViewController : UIViewController
    <UIPickerViewDelegate>{
        resItem *currentItem;
}
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) IBOutlet displaySizes *displaysList;
@property (strong, nonatomic) IBOutlet UITextField *xres;
@property (strong, nonatomic) IBOutlet UITextField *yres;
@property (strong, nonatomic) IBOutlet UITextField *diagonal;
@property (strong, nonatomic) IBOutlet UITextView *result;
@property (strong, nonatomic) IBOutlet UIView *pickerViewContainer;

- (IBAction)count:(id)sender;
- (IBAction)hidePicker:(id)sender;
- (IBAction)showPicker:(id)sender;
- (void)setValues:(resItem*)item;

@end
