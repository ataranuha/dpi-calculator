//
//  ResItemsController.m
//  test1
//
//  Created by Mac on 1/20/13.
//  Copyright (c) 2013 Mac. All rights reserved.
//

#import "resItem.h"
#import "ResItemsController.h"

@interface ResItemsController ()
@property (nonatomic, copy, readwrite) NSMutableArray *list;
- (void)createDemoData;
@end

@implementation ResItemsController
@synthesize list;

- (id)init {
    if (self = [super init]) {
        [self createDemoData];
    }
    return self;
}

- (void) setList:(NSMutableArray *) newList {
    if (list != newList) {
        list = [newList mutableCopy];
    }
}

- (unsigned) countOfList {
    return [list count];
}

- (resItem *) objectAtListInIndex:(unsigned)theIndex {
    return [list objectAtIndex:theIndex];
}

-(NSDictionary*)dictionaryWithContentsOfJSONString:(NSString*)fileLocation{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:[fileLocation stringByDeletingPathExtension] ofType:[fileLocation pathExtension]];
    NSData* data = [NSData dataWithContentsOfFile:filePath];
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data
                                                options:kNilOptions error:&error];
    // Be careful here. You add this as a category to NSDictionary
    // but you get an id back, which means that result
    // might be an NSArray as well!
    if (error != nil) return nil;
    return result;
}

- (void) createDemoData {
    NSDictionary *data = [self dictionaryWithContentsOfJSONString:@"resolutions.json"];
    
    NSMutableArray *itemList = [[NSMutableArray alloc] init];
    resItem *item;       
    for (id key in data)
    {
        id value = [data objectForKey:key];        
        item = [[resItem alloc] init];        
        item.name   = [value valueForKey:@"name"];
        item.resx   = [value valueForKey:@"resx"];
        item.resy   = [value valueForKey:@"resy"];
        item.aspect = [value valueForKey:@"aspect"];        [itemList addObject:item];
    }        
    
    self.list = itemList;    
}

@end
