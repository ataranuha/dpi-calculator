//
//  ResItemsController.h
//  test1
//
//  Created by Mac on 1/20/13.
//  Copyright (c) 2013 Mac. All rights reserved.
//
@class resItem;
#import <Foundation/Foundation.h>

@interface ResItemsController : NSObject
- (unsigned) countOfList;
- (resItem *) objectAtListInIndex:(unsigned)theIndex;
@end
