//
//  ViewController.m
//  test1
//
//  Created by Mac on 12/26/12.
//  Copyright (c) 2012 Mac. All rights reserved.
//

#import "ViewController.h"
#import "resItem.h"
#import "math.h"
#import "classes/dpi.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize pickerViewContainer;

- (id) init {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];    
    if (currentItem.resy) {        
        _yres.text = [NSString stringWithFormat: @"%@",currentItem.resy];        
        _xres.text = [NSString stringWithFormat: @"%@",currentItem.resx];
        self.navigationItem.title = currentItem.name;        
    } else {
        [_yres setText:[NSString stringWithFormat: @"%@",@"640"]];
        [_xres setText:[NSString stringWithFormat: @"%@",@"960"]];
        self.navigationItem.title = @"iPhone 4 retina";
    }
    [self count:self];
    pickerViewContainer.frame = CGRectMake(0, 460, 320, 261);
    //[[self navigationController] popToRootViewControllerAnimated:NO];
    
    
}

- (IBAction)count:(id)sender {
    dpi *dpiObj = [[dpi alloc] init];
    [dpiObj setYres: [_yres.text intValue]];
    [dpiObj setXres: [_xres.text intValue]];
    [dpiObj setDiag: [_diagonal.text floatValue]];
    [dpiObj recount];
    NSLog(@"Return: %@",[dpiObj resText]);
    [_result setText: [dpiObj resText]];    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showPicker:(id)sender {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    pickerViewContainer.frame = CGRectMake(0, 120, 320, 261);
    [UIView commitAnimations];
}

- (IBAction)hidePicker:(id)sender {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    pickerViewContainer.frame = CGRectMake(0, 460, 320, 261);
    [UIView commitAnimations];

}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {    
    return [_displaysList.sizes objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    _diagonal.text = [_displaysList.sizes objectAtIndex:[pickerView selectedRowInComponent:0]];
    [self count:self];
}

- (void)setValues:(resItem*)item{
    currentItem = item;
}

@end

