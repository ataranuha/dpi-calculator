//
//  siaplaySizes.m
//  DPI-calculator
//
//  Created by Mac on 2/6/13.
//  Copyright (c) 2013 Mac. All rights reserved.
//

#import "displaySizes.h"

@implementation displaySizes

- (id)init {
    if (self = [super init]) {        
        [self loadJsonData];
        [_sizes addObject:@"3"];
        [_sizes addObject:@"4"];
        [_sizes addObject:@"7"];
        [_sizes addObject:@"8"];
        [_sizes addObject:@"9.7"];
        [_sizes addObject:@"10.1"];
    }
    return self;
}

- (void) loadJsonData {
    NSDictionary *data = [self dictionaryWithContentsOfJSONString:@"displaySizes.json"];
    _sizes = [[NSMutableArray alloc] init];
    for (id key in data)
    {        
        [_sizes addObject:key];
    }
    NSLog(@"%@",data);
    [_sizes setArray: [_sizes sortedArrayUsingSelector:@selector(compare:)]];
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    return [_sizes count];}

-(NSDictionary*)dictionaryWithContentsOfJSONString:(NSString*)fileLocation{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:[fileLocation stringByDeletingPathExtension] ofType:[fileLocation pathExtension]];
    NSData* data = [NSData dataWithContentsOfFile:filePath];
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data
                                                options:kNilOptions error:&error];    
    if (error != nil) return nil;
    return result;
}

@end
