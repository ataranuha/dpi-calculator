//
//  resListController.m
//  test1
//
//  Created by Mac on 1/20/13.
//  Copyright (c) 2013 Mac. All rights reserved.
//

#import "ViewController.h"
#import "resItem.h"
#import "ResItemsController.h"
#import "resListController.h"
@implementation resListController
@synthesize resItemsController;

- (void) viewDidLoad {
    [super viewDidLoad];
    self.title = @"List";    
    ResItemsController *controller = [[ResItemsController alloc] init];   
    self.resItemsController = controller;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [resItemsController countOfList];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"itemCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];    
    resItem *itemAtIndex = [resItemsController objectAtListInIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat: @"%@", itemAtIndex.name];
    cell.detailTextLabel.text = [NSString stringWithFormat: @"%@x%@ %@", itemAtIndex.resx, itemAtIndex.resy, itemAtIndex.aspect];
    return cell;    
}

#pragma mark -
#pragma mark Table view selection

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *selectedRowIndex = [self.tableView indexPathForSelectedRow];
    ViewController *counterView = [segue destinationViewController];
    resItem *item = [resItemsController objectAtListInIndex:selectedRowIndex.row];    
    [counterView setValues:item];}


@end
